<?php
/**
 * 
 * OpenCore ORM
 *     
 * Dependencies: BaseSQL, Tables Model, SQLResultsCollection
 * 
 * @author guille


 * @version 1.2.11
 * @todo http://projects.zedplan.com:81/projects/opencore-php/issues
 *
 * @changeLog

 * 1.2.11 Se agrega improvement para manejo de comparador IN
 * 1.2.10 Se agrega variable protected preFilter y el metodo unsetFilter()
 * 1.2.9 Arreglo de faltantes de Redis Cache. By Galo.
 * 1.2.8 Se agrega opcion distinct() y se agregan try/catch al metodo find. gNan
 * 1.2.7 Se agrega metodo redisTTL para setear tiempo de vida de cache Redis
 * 1.2.6 Se agrega opcion de array como segundo parametro en filterBy
 * 1.2.5 Fix pequeño bug en save para cuando la tabla no tiene PK
 * 1.2.4 Se agrega metodo having()
 * 1.2.3 Se agrega borrador de getExec()
 */

require_once 'functions/camel.php';
import("db.BaseSQL");
import('db.Tables');
import('db.SQLResultsCollection');

class BaseSQLModel extends BaseSQL{

    const SAVE_MODE_UPDATE = 0b00000001;
    const SAVE_MODE_INSERT = 0b00000010;
    const SAVE_MODE_INSERT_OR_UPDATE = 0b00000011;
    const SAVE_MODE_REPLACE = 0b00000100;
    
    private   $model = null; // only used in case the model is not a real file.
    private   $customQuery = null; // string setted manually
    private   $query = null;  // string builded internally by ORM
    
    static private $tablesData = array(); // array('tablename'=>'primary key') . Used to store models pk only once, globally. 
    
    protected $conn = 'default';
    protected $table = null;
    protected $alias = null;
    protected $pk = null; // primary key. Recommended to be declared. Otherwise the ORM will tell
    protected $prefix = null;
    
    // this must be set using filterBy() method. And will result in this, by example.  
    private $options = array(
        'conditions' => array(        
//            'C.client_id' => array(
//                'value' => null,
//                'type' => "="
//            ) 
        ),
        'orderBy' => '',
        'groupBy' => '',
        'having' => '',
        'limit' => ''
    );
   
    /**
     * Each model must declare it's own relations. By example:
     * 
     * 'user' => array(
            'relationType' => 'INNER', // default
            'tableName' => 'Users',  // si no se declara, se toma la key del array
            'leftTableName' => '',  // por default
            'doJoin' => false,    // esto se pone en true cuando se usa el metodo join()
            'prefix' => null, // use only if all your fields share the prefix. Will be self-calculated if not provided
            'fields' => '*', // valor por default al usar el metodo join(). IMPORTANTE: si no se usa el metodo join, se toma la opcion desde el modelo y esto por default es null
            'formatedFields' => array(), // fields life DATEDIFF, COUNT, etc. 
            //'fk' => null,     // with or without prefix, depending on 'prefix' var
            //'foreignTablePk' => null, // if not provided, will be deduce using Tables class
            'joins' => array(), 
            'using' => '', // Ex: using => 'user_id'
            'on' => '',    // String. Ex: on => 'user_id = client_id'     
            'alias' => "CU"
        ),
        'clients' => array(
            'relationType' => 'LEFT',  
            'tableName' => 'Clients',
            'doJoin' => true, // join always by default
            'prefix' => null,
            'fields' => array(
                    'name', 
                    'last_name', 
                    'address'
                ),
            'alias' => "C"
        ),
        'cities' => array(
            'relationType' => 'INNER',
            'tableName' => 'Cities',
            'doJoin' => false,
            'prefix' => null,
            'alias' => "CT"
        )
     */
    protected $joins = array();
    
    
    /**
     * Each model can define filters that will be executed in every query. this preFilters can be overwritten.
     */
    protected $preFilters = array();
    
    protected $redisTTL = null; // redis cache TTL
    
    //protected static $instance = null; // singleton
    private $fields = array(); // internal use. Fields names and values associative array
    private $mainTableFields = array(); //fields of the main table
    private $retrieve_all_table_fields = true; // internal use. Retrieves all table fields by default
    private $retrieve_all_joins_fields = true; // internal use. Unless specified in field() method, return all fields of joined tables
    protected $columns_to_retrieve = array(); // These are the ones reflected in the SELECT [...] query.
    protected $formatedFields = array(); // fields that pass through fn like DATE_FORMAT. These are reflected in the SELECT [...] query.
        
    protected $tableFields = array(); // Table's columns names, without values. 
    private $queries = array(); // executed queries log 
    
    /* UPPERCASE reserved words */
    private $reservedWords = array('NOW', 'DATE', 'DATEADD', 'DATEDIFF', 'CONCAT', 'FORMAT', 'LEFT', 'LOWER', 'SUBSTR', 'TRIM', 'RTRIM', 'UPPER', 'CEIL', 'FLOOR', 'RAND', 'ROUND', 'MD5', 'SHA1', 'SHA2', 'SHA', 'ENCRYPT', 'SUM', 'COMPRESS', 'REPLACE', 'HOUR', 'MINUTE', 'SECOND', 'MONTH', 'YEAR', 'WEEK', 'WEEKDAY', 'DAY', 'TIMESTAMP', 'TO_DAYS', 'DAYOFWEEK', 'DAYOFYEAR');
    
    /**
     * Mode of sql save
     * 
     * @var int 
     */
    
    protected $saveMode = self::SAVE_MODE_UPDATE;
    
    /**
     * @param int PK
     * @param 
     * @param string $conn If null, 'default' is taken  
     */
    public function __construct() {
       
        try {
           
            $this->filterBy( $this->preFilters );
            
            $args = func_get_args();
            if (!empty($args) && !empty($args[0])) {
                       
                if (func_num_args() == 1 && is_numeric($args[0])) 
                    $args[0] = (int) $args[0]; // force int for primary key
                
                if(!empty($args[2]))
                    $this->conn = $args[2]; // set connection
                
                if (is_int($args[0])) {
                    // for instance: new Users(2);
                    $tableName = (!empty($this->table)) ? $this->table : get_called_class();
                    $this->_setTableName($tableName);
                    $this->_setPkName();
                    $this->findOneByPK($args[0]); // will use the column prefix if defined
                    
                    
                } else {  
                    // for instance: new BaseSQLModel('Clients');  
                    $this->_setModelName($args[0]); 
                    $this->_setTableName($args[0]); 
                    $this->_setPkName();   
                    
                    if(!empty($args[2]))
                        $this->conn = $args[2];
                    
                    // for instance: new BaseSQLModel('Clients', 3); 
                    if(!empty($args[1]))
                        $this->findOneByPK($args[1]);
                }
                
            } else {      
                
                // for instance: new Users();
                $tableName = (!empty($this->table)) ? $this->table : get_called_class();
                $this->_setTableName($tableName); 
                $this->_setPkName();        
            }
                       
            
        } catch (Exception $exc) {
            ///echo $exc;
            //fb($exc);
            throw new Exception($exc->getMessage());
        }
    }
      
    /**
     * Auxiliary method to implode arrays
     * 
     * @param array/string $data
     * @param string $glue
     * @return string
     */
    private function implodeIfArray($data, $glue = ', '){
                
        if(!is_array($data)){
            
            $result = $data;
        } else {
            $result = implode($glue, $data);
        }
            
        return $result;
    }
    
    /**
     * Sets a custom query 
     * Note you can still use filterBy(), limit(), orderBy() and groupBy() methods even if you wrote your custom query
     * @param type $query
     * @return \BaseSQLModel
     */
    public function setQuery($query) {
        
        $this->customQuery = $query;         
        
        return $this;
    }
      
    private function _getCustomQuery() {
        
        return empty($this->customQuery) ? false : $this->customQuery;  
    }
     
    /**
     * This builds the query on the fly, based on the defined data, and return a string.
     * It's intended to preview the query. If you want to retrieve executed queries by the current model, use getExecutedQueries() instead
     * @param bool $includeGroup
     * @param bool $includeOrder
     * @param bool $includeLimit
     * @return this
     */
    public function getQuery($includeGroup = true, $includeOrder = true, $includeLimit = true) {
        
        return $this->_buildQuery( $includeGroup, $includeOrder, $includeLimit );  
    }

    /**
     * Gets INSERT or UPDATE string based on defined pk value
     * @version alpha 1 - Galo 2013-10-2
     */
    public function getExec(){
        
        return $this->_buildExec();  
    }
    
    private function _setModelName($modelName) {
        
        $this->model = $modelName;  
    }
      
    private function _getModelName() {
        
        return $this->model;  
    }
      
    private function _setTableName($tableName) {
        
        $this->table = $tableName;  
    }
    
    public function getTableName() {
        
        return $this->table;  
    }
      
    /**
     * Sets primary key name, not the value.
     * @param type $tableName 
     */
    private function _setPkName() {
        
        if(!empty($this->pk)){
            
            if(is_array($this->pk)){
                foreach ($this->pk as $table => $pk){
                    self::$tablesData[ $table ] = $pk; // each model can define several tables => pk_names in $pk var
                }
            } else {
                
                self::$tablesData[ $this->table ] = $this->pk; // model declared self pk name
            }
            
        } elseif( empty(self::$tablesData[ $this->table ]) ) {
            
            self::$tablesData[ $this->table ] = Tables::getTablePk($this->table); // store pk name when creation models using new BaseSqlModel('model')
        }
    }
       
    /**
     * Array to override default values. 
     * Ex:
     *  'conditions' => array(        
     *       'C.client_id' => array(
     *           'value' => null,
     *           'type' => "="
     *       ) 
     *   ),
     *   'orderBy' => '',
     *   'groupBy' => '',
     *   'limit' => '25'
     * 
     * @param array $option 
     */
    protected function _setOptions($option)
    { 
        array_merge($this->options, $option);
        return $this;
    }
    
    protected function _getOption($key)
    {     
        return ireturn( $this->options[$key] );
    }
         
    /**
     * Array to set query conditions
     * Ex:
     * $conditions = array(        
     *       'C.client_id' => array(
     *           'value' => null,
     *           'type' => "="
     *       ) 
     * @param type $conditions 
     */
    private function _setConditions($conditions){
                
        $this->options['conditions'] = array_merge($this->options['conditions'], $conditions); 
          
        return $this;
    }
        
    private function _getConditions(){
        
        $conditions = array();
     
        foreach ($this->options['conditions'] as $key => $params)
        {
             
            if (!is_array($params)) {
                // parse string conditions (the ones not declared as arrays haves numeric keys)
                $conditions[] = $params;

            } 
            elseif (!isset($params['value'])){
                
                $type = ($params['type'] == '=') ? 'IS NULL' : 'IS NOT NULL';
                
                $conditions[] = "$key $type";
                
                //continue; // skip not wellformed conditions
            }          
            else {
                
                if( !array_key_exists('value', $params) && is_array($params) ){
                    
                    foreach ($params as $value) {
                        
                        $sign = (!empty($params['type']) ) ? $params['type'] : "=";
                        $value = BaseSQL::php2sql($params['value']);
                        $conditions[] = $key . ' ' . $sign . ' ' . $value  . ''; // quoting value
                    }
                    
                } else {
                                       
                    $sign = (!empty($params['type']) ) ? $params['type'] : "=";
                    $value = BaseSQL::php2sql($params['value']);
                    $conditions[] = $key . ' ' . $sign . ' ' . $value  . ''; // quoting value
                }
            }  
        }
        
        return $conditions;
    }
    
    private function _getConditionsString(){
        
        $conditions = implode(" AND ", $this->_getConditions());
        return (string) $conditions;
    }
              
    private function _getAlias(){
        return (!empty($this->alias)) ? $this->alias : $this->table;
    }
    
    private function _setTableFields(){
        $this->tableFields= Tables::getColumns($this->table);
    }
    
    /**
     * Return table fields of the instanciated model
     */
    private function _getMainTableFields(){
        
        if( empty( $this->mainTableFields ) )
            $this->mainTableFields = Tables::getColumns($this->table);
        
        return $this->mainTableFields;
    }
    
    private function _getJoins(){
        
//        $alias = $this->_getAlias();
        
        $columns = $joins = array();
          
        // declare fields using table alias
        foreach ($this->joins as $key => $model) {
            
            $model = json_decode (json_encode ($model), FALSE); // array to object tweak
            
            $doJoin = ( !empty($model->doJoin) ) ? $model->doJoin : false; // note a relation may be declared in a model, but not used in every query. That's why this exists.
            
            if($doJoin)
            {                    
                $joinOn = '';          
                $rel_type = ( !empty($model->relationType) ) ? $model->relationType : 'INNER';
                $rel_table = ( !empty($model->tableName) ) ? $model->tableName : $key;
                $rel_alias = ( !empty($model->alias) ) ? $model->alias : $rel_table; // provide an alias if none.
                
                if(!empty($model->on) || !empty($model->using)){
                    
                    if(!empty($model->on)){
                        $joinOn = " ON ".$model->on;
                    } else {
                        $joinOn = " USING ({$model->using}) ";
                    }
                    
                } 
                
                if($this->retrieve_all_joins_fields){
                    
                    // get all fields or just some?
                    if( isset( $model->fields ) ){

                        if(is_object($model->fields))
                        $model->fields = get_object_vars($model->fields);

                        if(is_array($model->fields)){

                            foreach ($model->fields as $key => $value) {

                                if(is_numeric($key)){

                                    $columns[] = $rel_alias.".$value"; // restric fields if user wants to, instead of retrieving all
                                } else {

                                    $columns[] = $rel_alias.".$key AS $value"; // restric fields if user wants to, instead of retrieving all
                                }

                            }
                        } else {

                            $columns[] = $model->fields;
                        }

                    } else {
                        $columns[]= $rel_alias.'.* '; // retrieve all table fields
                    }

                    if(!empty($model->formatedFields)){

                        if(is_array($model->formatedFields)){

                            foreach ($model->formatedFields as $field) {
                                $columns[] = $field; // restric fields if user wants to, instead of retrieving all
                            }
                        } else {

                            $columns[] = $model->formatedFields;
                        }
                    }
                }
            
                // SQL string
                $join = " $rel_type JOIN $rel_table $rel_alias "; 
               
                if( !empty($joinOn) ){
                    $join.= $joinOn;
                }
//                else{
//                    $join.= " ON $alias.$FK = $rel_alias.$fTablePK ";
//                }
                        
                $joins[] = $join;
            }    
        }   
        
        if(!empty($columns))
            $this->columns_to_retrieve[] = implode(',', $columns ); 
        
        return $joins;
    }
    
    /**
     * Returns the name of the keys of the table(s)
     * @return type
     */
    public function getTableFields($addAlias = false){
        
        if(empty($this->tableFields))
            $this->_setTableFields();
            
        if($addAlias){
            $tableFields = array();
            $alias = $this->_getAlias();
            foreach ($this->tableFields as $value) {
                $tableFields[] = "$alias.$value";
            }
            return $tableFields;
        }
        
        return $this->tableFields;
    }
    
    /**
     * @method int set() set(string $field, string $value) set values
     * @method int get() get(string $field) get values
     * //method SQLResultsCollection findOne() findOne(string $columnName, $value) 
     * //method $this filterBy() filterBy(string $columnName, $value) 
     * //method $this findOneBy() findOneBy(string $columnName, $value) 
     * //method $this findBy() findBy(string $columnName, $value) 
     * @param string $method
     * @param $args  
     */
    public function __call($method, $args) {
            
        if(!method_exists($this, $method))
        { 
            if( in_array( substr($method, 0, 3) , array("set", "get")) ){
                
                /* GET and SET */
                $methodType = substr($method, 0, 3);
                
                if(count($args) == 2){
                    $fieldName =  $args[0];
                    $value = $args[1];
                }
                else{
                    $fieldName = substr($method, 3);
                    $value = !isset($args[0]) ? null : $args[0]; // TODO: allow get('field_name')
                }

                if ($methodType == "set") { 
                    $this->_setFieldValue($fieldName, $value);
                }

                if ($methodType == "get") {
                    return $this->getField($fieldName);
                }  
            } 
            elseif( substr($method, 0, 8) == "filterBy" ){
                
                /* filterBy */
                $methodType = substr($method, 0, 8);
                $fieldName = substr($method, 8);
                
                if($fieldName=="PK")
                    $fieldName = $this->getPkName(); 
                
                return $this->filterBy( from_camel_case($fieldName) , $args[0] );               
            }
            elseif( substr($method, 0, 9) == "findOneBy" ){ 
                
                /* FindOneBy */
                $methodType = substr($method, 0, 9);
                $fieldName = substr($method, 9);
                if($fieldName=="PK")
                    $fieldName = $this->getPkName();   
                
               // pr($fieldName);
                $temp = from_camel_case($fieldName);
                return $this->findOneBy( from_camel_case($fieldName) , $args[0]);
            }
            elseif( substr($method, 0, 7) == "findOne" ){
                /* FindOne */
                $methodType = substr($method, 0, 7);
                $fieldName = substr($method, 7);
         
                if(!empty($args[0])){
                    $column = $this->getPkName();   
                  
                    return $this->findOne($column, intval($args[0])); //get One by Pk 
                }

                return $this->findOne(); //get One by defined conditions 
            } 
            elseif( substr($method, 0, 6) == "findBy" ){ 
                
                /* findBy */
                $methodType = substr($method, 0, 6);
                $fieldName = substr($method, 6);
                
                if($fieldName=="PK")
                    $fieldName = $this->getPkName();   
                
               // pr($fieldName);
                return $this->findBy( from_camel_case($fieldName) , is_null($args[0]) ? self::php2sql($args[0]) : $args[0]);
            }
            elseif( substr($method, 0, 8) == "deleteBy" ){ 
                
                /* deleteBy */
                $methodType = substr($method, 0, 8);
                $fieldName = substr($method, 8);
                
                if($fieldName=="PK")
                    $fieldName = $this->getPkName();   
                
               // pr($fieldName);
                return $this->deleteBy( from_camel_case($fieldName), is_null($args[0]) ? self::php2sql($args[0]) : $args[0] );
            }
            else  {
                $msg = 'Method not found: '.$method."\n";
                //echo $method;
                throw new Exception( $msg );
            }
                
        } else return $this->$method($args);
    }
            
    /**
     * Version 0.2 2013-6-12 Guille
     * @return type
     */
    protected function getPkName() { 
        
        return ireturn( self::$tablesData[ $this->table ] );
    }
    
     /** 
     * @version 1 - Galo 2013-10-2
     */
    private function getPkValue() { 
        
        $pk = $this->getPkName();
        $pkValue = null;
        $data = $this->getData();

        if(!empty($pk))
            $pkValue = ireturn($data[$pk]);
        
        return $pkValue;
    }
         
    protected function getTablePrefix() { 
        
        return ireturn($this->prefix);
    }
        
    public function conn($value){
        
        $this->conn = $value;      
        return $this;
    }
    
    /**
     * Returns an instance of this class. This is not Singleton.
     *
     * @return self
     * @version 0.1 2013-6-11 Guille
     * @
     * @
     */
    public static function createInstance($tableName = null, $pk_id = null, $conn = null) {
      
        return new static($tableName, $pk_id, $conn);
    }
           
    /**
     * Returns a singleton instance of this class.
     *
     * @return self
     */
//    public static function getInstance() {
//       
//        if (!isset(static::$instance)) {
//            static::$instance = new static();
//        }
//
//        return static::$instance;
//    }
    
    /**
     * 
     * 
     *  $user->join( 'UserFavorities' ); // one join example
     * 
     *  $user->join( array(
     *      'UserFavorities'=>'user_id'
     *  )); // one join declaring FK name
     * 
     *  $user->join( array(
     *      'UserFavorities',
     *      'UserTemplates'
     *  )); // multiple joins. 
     */
    public function join(){
        
        $args = func_get_args();
        $options = array();      
        
        if(is_array($args[0])){
            
            foreach ($args[0] as $relationName => $options) {
                
                if(is_numeric($relationName)){
                    
                    $relationName = $options; // if numeric index signifies they didn't declare any options
                    $options = null;
                } elseif(!is_array($options)) {
                    $options['fk'] = $options; 
                }
                
                $this->_doJoin($relationName, $options);
            }
            
        } elseif(!empty($args)) {
            
            $options['fk'] = @$args[1];
            $this->_doJoin($args[0], $options);
        }
        
        return $this;
    }
    
    private function _doJoin($relationName, $options = array()){
     
        $this->joins[$relationName]['doJoin'] = true;
        
        if(!empty($options)){
            $this->joins[$relationName] = array_merge( $this->joins[$relationName], $options );
        }
    
        $rel_alias = ( !empty($this->joins[$relationName]['alias']) ) ? 
                $this->joins[$relationName]['alias'] : $relationName; // provide an alias if none.
         
        if( !isset($this->joins[$relationName]['fields']) )
            $this->joins[$relationName]['fields'] = "$rel_alias.*"; // select all fields by default
        
        if(!empty($options['fk']))
            $this->joins[$relationName]['fk'] = $options['fk']; // if fk is different from target table PK, we'll use ON syntax. Otherwise, we prefer USING()
    }
    
    /**
     *  $userModel = new User();
     *  $users = $userModel->filterBy("Date", "15/3/2010")->find();
     *  $users = $userModel->filterBy("Date='15/3/2010'")->find();
     *  $users = $userModel->filterByDate('15/3/2010')->find();
     *  $users = $userModel->findByDate("15/3/2010");
     * 
     * @param array $conditions
     * @return \BaseModel 
     */
    public function filterBy( /* $filters, $value = null */ ){
       
        $args = func_get_args();
                
        try {
            
            // if not array given, generate one, an merge it with existing options latelly
            if(!is_array($args[0])){

                if (func_num_args() == 1 && is_numeric($args[0])) 
                    $args[0] = (int) $args[0]; // force int for primary key
                
                if(is_int($args[0])){
                    // example: $user->find(5);
                    $alias = $this->_getAlias();
                    $key = $alias.'.'.$this->getPkName();
                    $conditions = array(        
                        $key => array(
                            'value' => is_null($args[0]) ? null : "'".$args[0]."'",
                            'type' => "="
                        )
                    );

                } else {
                    
                    if(!empty($args[1])){ 
                        
                        if(is_array($args[1])){
                            // example: $user->filterBy("user_id", array(3, 5, 7));    
                            $values = '"' . implode('", "', $args[1]) . '"';
                            
                            $conditions[ $args[0] ] = array(
                                'value' => "($values)",
                                'type' => "IN"
                            );
//                            fb($conditions);
                        } else {
                            // example: $user->filterBy("user_name", "Carlitos");
                            $conditions = array(        
                                $args[0] => array(
                                    'value' => is_null($args[1]) ? null : "'".$args[1]."'",
                                    'type' => "="
                                ));
                        }
                    } else {
                        
                        // example: $user->filterBy("user_name = 'Carlitos'");
                        $conditions = array($args[0]); // this is an arbitrary string
                    }
                }   
                
                $this->_setConditions( $conditions );
                
            } else {

                // Examples:
                // 
                // $user->find(array('user_id'=>5));
                // 
                // $user->filterBy(
                //           'C.client_name' => array(
                //                'value' => 'Ramiro',
                //                'type' => 'LIKE'
                //            ) 
                //           'client_city' => 3
                //  );
                // $user->filterBy(
                //           'C.client_name' => array(
                //                'value' => NULL,
                //                'type' => 'IS NOT'
                //            ) 
                //  );
                // $user->filterBy( 
                //          array(
                //              'user_id IS NOT IN (5)',
                //              'issue_date > "2013-6-12"'
                //          ) 
                //  );
                //  
                //  // multiple conditions for same column (TODO. Unfinished. Guille 7-2013)
                //  $user->filterBy(
                //        'C.c_date' => array(
                //             array(
                //                 'value' => '2013-9-3',
                //                 'type' => '>='
                //             ),
                //             array(
                //                 'value' => '2013-5-1',
                //                 'type' => '<='
                //             )
                //         ) 

                foreach ($args[0] as $key => $value) {

                    if(is_numeric($key)){
                        
                        $conditions = array( $value );
                    }
                    elseif(!is_array($value)){
                                                
                        $conditions[$key] = array(
                                    'value' => is_null($value) ? null : "'".$value."'",
                                    'type' => "="
                                );
                    } else {
                        
                        if(!array_key_exists('value', $value) && is_array($value)){
                            
                            if(is_array($value)){
                               
                                // example: $user->filterBy("user_id", array(3, 5, 7));    
                                $values = '"' . implode('", "', $value) . '"';

                                $conditions[ $key ] = array(
                                    'value' => "($values)",
                                    'type' => "IN"
                                );
                            }
                            
                            // parse multiple conditions for same key (TODO. Unfinished. Guille 7-2013)
//                            foreach ($value as $val){
//                                
//                                $val['value'] = is_null($val['value']) ? null : "'".$val['value']."'";
//                                $conditions = array( $key => $val );
//                                $this->_setConditions( $conditions );
//                            }
//                            continue;
                            
                        } else {
                               
                            $value['value'] = is_null($value['value']) ? null : "'".$value['value']."'";
                            $conditions = array( $key => $value );
                        }
                    }
                    
                    $this->_setConditions( $conditions );
                    
                }

            }
          

        } catch (Exception $exc) {
//            fb($exc); // temp
            throw new Exception($exc->getMessage());
        }
        
        return $this;
    }
         
    public function filterByPK($pk){
        
       return $this->filterBy( $pk );
    }
    
//    public function filterExists(){
//        
//        // esto solo devolvería isset para filtros que se setearon como array. No funcionaría con los seteados como string :/
//    }
    
    /**
     * Find one item 
     * @param string $column
     * @param type $value
     * @return null 
     * 
     * Examples:
     *  $userModel = new User();
     *  $users = $userModel->filterBy("Date='15/3/2010'")->findOne();
     *  $users = $userModel->filterByDate('15/3/2010')->findOne();
     *  $users = $userModel->findOneByDate("15/3/2010");
     */
    public function findOne(/*$column = null, $value = null*/) {
               
        $args = func_get_args();        
        
        if(!empty($args[0])){
            $val = (func_num_args() == 1) ? (int) $args[0] : (string) $args[0]; // force int for primary key
            $this->filterBy($val, @$args[1]);   
        }            
                    
        $this->setOption('limit', 1);
      
        return $this->find();                    
    }
    
    public function findBy( /*$column = null, $value = null*/ ){
         
        $args = func_get_args();
        
        if(!empty($args[0]))
            $this->filterBy(@$args[0], @$args[1]);

        return $this->find();
    }
    
    /**
     * Examples:
     *  $userModel = new User();
     *  $users = $userModel->findOneBy('date', "15/3/2010");
     *  $users = $userModel->findOneByDate("15/3/2010");
     * 
     * @param type $column
     * @param type $value
     * @return type 
     */
    public function findOneBy($column = null, $value = null){
            
        return $this->findOne($column, $value);
    }
    
    /**
     * Find a collection of items
     * @return array Object Collection 
     */
    public function find($filters = null)
    {        
        try{
            
            if(!empty($filters))
                $this->filterBy($filters);

            $sql = $this->_buildQuery();

           // fb( $sql );

            // LIMIT and PAGINATION
            if( $this->_getOption('limit') == 1 ){ 

                $this->_logQuery($sql);

                $result = self::doIQuery($sql, true, $this->conn, $this->redisTTL);

                if($result)
                     $this->populateFields( $result, ($this->_getCustomQuery() !== false) );

                return $this;

            } else {

                $collection = new SQLResultsCollection( $this->getPkName() ); // create a collection object

                $this->_logQuery($sql);

                $paginateOpts = $this->_getOption('paginate');   

                if(isset($paginateOpts['order'])) {
                    $pagiOrder = $paginateOpts['order'];
                    if(!empty($paginateOpts['direction']))
                        $pagiOrder.= " ".$paginateOpts['direction'];  
                } else {
                    $order  = $this->_getOption('orderBy');
                }

                if ( !empty($pagiOrder) ){
                    $sql .= " ORDER BY $pagiOrder"; 
                }

                if( !empty($paginateOpts) ){

                    $page = (isset($paginateOpts['page'])) ? $paginateOpts['page'] : 1;
                    $resultsPerPage = (!empty($paginateOpts['resultsPerPage'])) ? $paginateOpts['resultsPerPage'] : 25;

                    $rs = DB::getConnection($this->conn)->query($sql, $resultsPerPage, $page);

                    $collection->paginate($rs, $paginateOpts);                

                    $result = $rs->fetchAll();
                }
                else{

                    $limit = $this->_getOption('limit');
                    if(!empty($limit))
                        $sql .= " LIMIT $limit";

                    $result = self::doIQuery($sql, null, $this->conn, $this->redisTTL);

                }

                if($result){

                    foreach ( $result as $data ){ 

                        $obj = new static( $this->_getModelName() );

                        $obj->populateFields( $data, $this->_getCustomQuery() !== false );
                        $collection->add($obj);
                    }                
                }

                return $collection;         
            }
        } catch( Exception $e ){
            throw $e;
        }
        
    }
    
    private function _buildQuery( $includeGroup = true, $includeOrder = true, $includeLimit = false ){
                
        $table = $this->table;
        $alias = $this->_getAlias();
                   
        if($this->_getCustomQuery() === false){
            
            $joins = $this->_getJoins(); // joins 
            
            $sql = "SELECT ";
            
            if( $this->_getOption( 'distinct' ) ) $sql .= "DISTINCT ";
            
            $formatedFields = $this->formatedFields; 
            
            if(!empty($this->columns_to_retrieve)){
                
                $fields_to_retrieve = $this->columns_to_retrieve;
                 
                if($this->retrieve_all_table_fields){
                   
                    
                    $mainFields = (!empty($this->tableFields)) ? $this->getTableFields(true) : array("$alias.*");
                    $fields_to_retrieve = array_merge( $mainFields, $formatedFields, $fields_to_retrieve );
                }
                
                $sql .= implode(', ', $fields_to_retrieve);
            }
            else{
                
                $sql .= "$alias.*"; // get all from main table
                
                if(( !empty($formatedFields)) )
                    $sql .= ",". implode(', ', $formatedFields);
            }
                        
            $sql .= " FROM $table $alias ";
            
            $sql .= implode(' ', $joins);
          
        } else {
            
            $sql = $this->_getCustomQuery();
        }
                      
        // conditions
        $conditions = $this->_getConditionsString(); // conditions string
        if(!empty($conditions)){
           
            $sql .= ' WHERE '.$conditions;
        }
        
        // GROUP BY
        if($includeGroup){
            
            $group_criteria = $this->_getOption('groupBy');
            if ( !empty($group_criteria) ){
                $sql .= " GROUP BY $group_criteria"; 
                
                $having = $this->_getOption('having');
                if ( !empty($having) ){
                    $having = $this->implodeIfArray($having);
                    $sql .= " HAVING $having "; 
                }
            } 
        }
        
        // ORDER BY . This not represents paginators option, but the option setted using orderBy() method.
        if($includeOrder){
            
            $order = $this->_getOption('orderBy');
            if ( !empty($order) ){
                $sql .= " ORDER BY $order"; 
            } 
        }
        
        // LIMIT 
        if($includeLimit){
            
            $limit = $this->_getOption('limit');
            if(!empty($limit))
                $sql .= " LIMIT $limit";
        }
        
        return $sql;
    }
       
    private function _buildExec( $secureSaving = false ){
        
        $db = QueryBuilder::create();
        
        $table = $this->table;

        if($secureSaving){

            $data = $this->getData( $this->_getMainTableFields() );
        } else {

            $data = $this->getData( );
        }

        $pk = $this->getPkName();
        $pkValue = $this->getPkValue();

        if(empty($pkValue) || $this->saveMode == self::SAVE_MODE_INSERT){

            //insert
            $sql = $db->createInsert($table, $data);
        } else {

            //save
            
            switch ($this->saveMode) {
                case self::SAVE_MODE_UPDATE:
                    $wherePK = $pk.' = '.$pkValue;
                    $sql = $db->createUpdate($table, $data, $wherePK);
                    break;
                case self::SAVE_MODE_INSERT_OR_UPDATE:
                    $sql = $db->createInsertOrUpdate($table, $data);
                    break;
                case self::SAVE_MODE_REPLACE:
                    $sql = $db->createReplace($table, $data);
                    break;
                default:
                    throw new Exception("Unknow save mode");
            }
            
            
        }
        
        return $sql;
    }
    
    /** 
     * Only for options, not for conditions 
     * Ex: 
     * setOption('orderBy', 'table_id ASC')
     * setOption('groupBy', 'table_id')
     * setOption('limit', '25')
     * 
     * However, you can pass an array as second param to overridde conditions. 
     * 
     * 
     * @param string $key 
     * @param string $value 
     */
    public function setOption($key, $value)
    {        
        $this->options[$key] = $value;    
        return $this;
    }
        
    /**
     * Prepares ORM to paginate. Use find() after this.
     * 
     * Options:
     * $paginateParams = array(
            'action' => $input['action'],                   // url to link to (ex: /users/search)
            'resultsPerPage' => $input['resultsPerPage'],   // expected results per page
                  'numItems' => $input['numItems'],         // items in scroller, at sides of current page
                     'order' => $input['order'],
                      'page' => $input['pageNumber']
                    'uriVar' => default pageNumber          // name given to 'current page' var
                   'ulClass' => default ulClass             // css class for generated <ul>
        );
     * 
     * @return \BaseSQLModel
     */
    public function paginate($params){
        
        $this->setOption('paginate', $params); 
        
        return $this;
    }
    
    public function distinct(){
        
        $this->setOption('distinct', true); 
        
        return $this;
    }
    
    /**
     * Sets LIMIT condition for query
     * @param int $value
     * @return \BaseSQLModel 
     */
    public function limit($value){
        
        $this->setOption('limit', intval($value));      
        return $this;
    }
    
    /**
     * Sets ORDER BY condition for query
     * You can either pass one param (like 'id ASC') or two: ('my_id', 'DESC')
     * 
     * @param string $value
     * @return \BaseSQLModel 
     */
    public function orderBy(/*$value*/){
        
        $value = (func_num_args() == 2) ? implode(" ", func_get_args()) : func_get_arg(0);
                    
        $this->setOption('orderBy', strip_tags($value));      
        return $this;
    }
    
    /**
     * 
     * @param array o string $value
     * @return \BaseSQLModel
     */
    public function groupBy($value){
           
        $this->setOption('groupBy', $value);      
        return $this;
    }
    
    /**
     * Galo 2013-10-2
     */
    public function having($value){
           
        $this->setOption('having', $value);      
        return $this;
    }
        
    /**
     * Generates model fields based on database columns. 
     * From now, you can use getWhatever() to retrieve the field you want.
     * @param type $fields 
     */
    private function populateFields($fields, $userDefinedQuery = false) { 
        
        try {
               
            foreach ($fields as $key=>$field) {
                
                if($userDefinedQuery && !in_array($key, $this->tableFields))
                    $this->tableFields[] = $key;
                 
                $method = "set" . to_camel_case($key, true); 
                $this->$method($field);
            }
        } catch (Exception $exc) {
           
            fb ($exc); // temp
            throw new Exception($exc->getMessage());            
        }        
    }
      
    public function bindForm($form) {
       
        if(!is_array($form)){
            $form = $form->getData();
        }
       
        $this->_setFieldsValues($form);
    }
      
    /**
     * 
     * @param type $dataArray
     */
    private function _setFieldsValues($fieldsArray){
       
        foreach ($fieldsArray as $key=>$value) { 
            $this->_setFieldValue($key, $value);
        }
    }
    
    private function _setFieldValue($fieldName, $value) {
        
//        if ( !$this->checkFieldExist(from_camel_case($fieldName)) ){
//            throw new Exception("The Field:'$fieldName' do not exist in the table.");
//        }
           
        $this->fields[ from_camel_case($fieldName) ] = $value;
    }
    
    /**
     * Unset field from internal storage
     * @param string $fieldName
     */
    public function unsetField($fieldName){
  
        unset( $this->fields[ $fieldName ] );
    }
    
    /**
     * Unset filter from internal storage, usefull to unset filters
     * @param string $fieldName
     */
    public function unsetFilter( $filterKey ){
        
        try{
            
            unset( $this->options['conditions'][ $filterKey ] );

            return $this;
            
        } catch( Exception $e ){
            throw $e;
        }
        
    }
    
    /**
     * Unset fields from internal storage
     * @param array $fieldArray
     */
    public function unsetFields($fieldArray){ 
        foreach($fieldArray as $fieldName)
            $this->unsetField($fieldName);
    }
    
    public function saveForm($inputs = null){
        
        if(empty($inputs))
            $inputs = $this->getData();
        
        foreach( $inputs as $key => $val ){  
            if( in_array( $key, (array) $this->_getMainTableFields() ) )
            {
                $this->set( $key, ( is_null($val) ? null : self::doIQuote($val) ) );
            }          
        }
        
        return $this->save();
    }

    /**
     * 
     * @deprecated since 2013-6-12 by Guille because this forces join to declare there table columns as well, slowing down the system
     * 
     * @param type $fieldName
     * @return type
     */
    private function checkFieldExist($fieldName){
        return in_array($fieldName, $this->tableFields);
    }
    
    /**
     * Get field From Result
     * @param type $fieldName
     * @return type
     */
    public function getField($fieldName) {
     
        $column = (!empty($this->prefix)) ? $this->prefix."_".from_camel_case($fieldName) : from_camel_case($fieldName); 
        return ireturn($this->fields[$column]);
    }
    
    /**
     * Declares the columns you want to retrive from table(s)
     * Note you can declare main table's fields as array, or a string. If you prefer the string the you can:
     * 1) use complex annotations, like: 'DATE_FORMAT(C.name)'
     * 2) ask for fields from another table
     * 
     * @param string or array $columns
     * @param bool $justTheseFields Don't retrieve any other field from main table 
     * @param bool $noJoinsFields Don't retrieve any other field from joins
     *
     */
    public function fields($columns, $justTheseFields = true, $noJoinsFields = false) {
        
        if(!empty($columns)){
            
            $this->retrieve_all_table_fields = !$justTheseFields;

            $this->retrieve_all_joins_fields = !$noJoinsFields;

            if(!is_array($columns)){

                $this->columns_to_retrieve[] = $columns;
            } else {
                $this->columns_to_retrieve[] = implode(', ', $columns);
            }
        }
        
        return $this;
    }
        
    public function formatedFields($formatedFields, $overwrite = false){
        
        if($overwrite){
            
            $this->formatedFields = $formatedFields;
        }
        else{
            
            $this->formatedFields = array_merge( (array)$this->formatedFields, (array)$formatedFields );
        }
        
        return $this;
    }
    
    public function getPK(){
        return $this->fields[ $this->getPkName() ];
    }
    
    /**
     * Only for findOne queries
     */
    public function isEmpty(){
        
        return empty($this->fields);
        
    }
    
    /**
     * Gets result data as array
     * @filter return the data only maching the keys given by parameter
     * @return type
     */
    public function getData( $filter = array() ){
        
        if( empty( $this->fields ) )
            return false;
        
        if( empty( $filter ) ){
            return $this->fields;
        }
        else{       
            
            $fields = $this->fields;
           
            $mainTableFields = $this->_getMainTableFields();            
            
            foreach( $fields as $key => $val ){
                
                if( !in_array( $key, $mainTableFields ) )                  
                        unset( $fields[ $key ] );
            }
            
            return $fields;            
        }
    }
          
    /**
     * Retrieves just one field named as wanted. 
     * 
     * @param string $alias
     * @param bool $justThisField By default, we retrive only this field from main table. One column result, let's say.
     * @param bool $noJoinsFields Same as $justThisField but regarding to joins. By default, we don't want any join to retrieve additional fields. 
     * 
     * @version beta 1.1 by Galo 30/8/2013
     */
    public function count($alias = 'count', $justThisField = true, $noJoinsFields = true){
        
        $this->fields("count(*) as $alias", $justThisField, $noJoinsFields);
        return $this->findOne();
    }
    
    /**
     * Alias for getData(). Note however that getData() accepts a param to filter columns.
     * @return array
     */
    public function toArray(){
        
        return $this->getData();
    }

    /**
     * By now, it only updates considering pk, not any other condition
     * 
     * @param bool $secureSaving Makes sure your fields matches the columns in database. It's just a SELECT before the INSERT or UPDATE.
     * @return boolean 
     */
    public function save( $secureSaving = true ) {
        
        try {
             // uncomment this
            $sql = $this->_buildExec( $secureSaving );
            $pk = $this->getPkName();
            $pkValue = $this->getPkValue();
                       
            
            $this->_logQuery($sql);
            $result = self::doIExec($sql, $this->conn);
            
            if( !empty($pk) && empty($pkValue) && $result !== FALSE)
                $this->setField($pk, $result);
          
            return $result;
            
        } catch (Exception $exc) {
            throw new Exception(_('Error al guardar ('.$exc->getMessage().')'));
            //prd($exc); // temp
            //return false;
        }        
    }
        
    public function deleteBy( /*$column = null, $value = null*/ ){
         
        $args = func_get_args();
      
        return $this->delete(@$args[0], @$args[1]);
    }
    
    /**
     * By now, it only erases considering pk, not any other condition
     * You can pass a single value or multiple values
     * 
     * @return boolean 
     * @version 0.2 Guille
     */
    public function delete(/*$colName, $colValue*/) {
        
        try {
       
            $table = $this->table;
            $args = func_get_args();
            
            if( !empty($args[1]) ){
                
                $colValue = $args[1];
                $colName = $args[0];            
            } else {
                
                $colName = $this->getPkName();   
                
                if(empty($colValue)){
                    $data = $this->getData();                
                    $colValue = ireturn($data[$colName]);
                }
            }

            if(!empty($colValue)){
                
                $sql="DELETE FROM $table ";
                
                if(!is_array($colValue)){
                    
                    $sql.=" WHERE $colName = $colValue";
                } else {
                    
                    $colValue = implode(', ', $colValue);
                    $sql.=" WHERE $colName IN ($colValue)";
                }
               
                //fb($sql);   
                $this->_logQuery($sql);
                return self::doIExec($sql, $this->conn);
            }
            
        } catch (Exception $exc) {
            throw new Exception(_('Error al eliminar ('.$exc->getMessage().')'));
            //fb($exc); // temp
            return false;
        }        
    }
        
    private function _logQuery($query){                
       
        $this->queries[] = $query;
    }
        
    public function getExecutedQueries(){
        
        return implode ("\n", $this->queries);
    }
        
    public function getLastExecutedQuery(){
        
        return end($this->queries);
    }
    
    public function execute(){
        
        self::doIExec( $this->getQuery(), $this->conn );
        return $this;
    }
    
    public function beginTransaction(){
        
        DB::getConnection($this->conn)->beginTransaction();
        return $this;
    }
    
    public function commit(){
        
        DB::getConnection($this->conn)->commit();
        return $this;
    }
    
    public function rollBack(){
        
        DB::getConnection($this->conn)->rollBack();
        return $this;
    }
    
    /**
     * Redis cache TimeToLive
     * @param type $ttl
     */
    public function redisTTL($ttl){
        
        $this->redisTTL = $ttl;
        return $this;
    }
    
    /**
     * Empty object data
     * 
     * @param int $deepness 1 / 3
     * 
     * @version beta 0.2 2013-9-27 Guille
     * @return self
     */
    public function clear($deepness = 1){
        
        if($deepness>2){
            $this->model = null;
            $this->tablesData = array();
            $this->conn = 'default';
            $this->table = null;
            $this->alias = null;
            $this->pk = null;
            $this->prefix = null;
        }
        
        if($deepness>1){
            
            $this->mainTableFields = array();
            $this->formatedFields = array();
        }
        
        $this->options = array(); 
        $this->query = null; 
        $this->customQuery = null; 
        $this->pk = null; 
        $this->joins = array(); 
        $this->fields = array(); 
        $this->queries = array(); 
        $this->tableFields = array();
        $this->columns_to_retrieve = array();
        $this->retrieve_all_table_fields = true;
        $this->retrieve_all_joins_fields = true;
        
        return $this;
    }
    
}



//        user_username
//
//        getUserUsername
//
//
//            $user= BaseModel::Create('Users');
//        
//        
//            $user->find(array('user_id'=>5));
//            $user->find(5);
//
//            $client= BaseModel::Create('Clients');
//            $client->find(70);
//
//
//            $cuit= $client->getCuit();
//
//            $client->setClientPhonenumber('15-3007-1121')->all();
//
//
//
//            Client_Phone_Number
//            client_phone_number
//            client_phonenumber
//
//            $client->save();
//
//
//        pr()
//
//            
//SELECT * FROM Users AS U
//            INNER JOIN UserFavorities AS UF ON U.user_id = UF.user_id
//  
//                
//SELECT * FROM Users U
//    INNER JOIN UserFavorities UF USING(user_id)
// 
//                
//                
//                
//    2)            
//     $user->join('UserFavorities')->on('user_id');
//    
//    1) 
//     $user->join( array('UserFavorities'=>'user_id') );//USING
//     $user->join('UserFavorities.user_id', 'Country.country_id');//USING
//              
//              
//     
//     $user = new BaseModel('Users');
//     $user->setUsername('mike');
//     
//     $user->save();
//     
//     $user->destroy();
//     unset($user);
//     
//     
//     
//     $userModel = new User();
//     $users = $userModel->filterBy("Date='15/3/2010'")->find();
//     $users = $userModel->filterByDate('15/3/2010')->find();
//     $users = $userModel->findByDate("15/3/2010");
//     
//     foreach($users as $user){
//         
//         
//         
//         
//     }
//     
//     $users->setUsername('pepe');
//     $users->save();
//     
//     // metodos utiles para una coleccion: first(), last(), isOdd(), get(0), save, delete
//     
//     $user->save();
//     
//     fb($users);
//     fb($user['collection'][0]->save());
//     
//     echo $user->getUsername();
//     
     
     
//
// * 2)
// * $user->filterByUserAge(45)->filterByUserActive(1);
// * 
// * 1)
// *$user->filterby($conditions)->find();
// * 
// *            $conditions = 5;
// *            $conditions = "age=45 AND active=1 date>'30/09/2012";
//
//              $conditions = array(
//                  'C.client_id' => 5,  
//                  'C.client_active' => 1, 
//                  'C.client_date' => array( 
//                      'velue'=> '30/09/2012' , 
//                      'type' => '>'
//                )); 
// *  
 




?>


