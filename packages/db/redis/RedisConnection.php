<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2013, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2013, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 * @
 */

// namespace db\redis;



/**
 * Represents a redis database connection.
 *
 * @package db.redis
 * @author ZedPlan Team (opencorephp@zedplan.com)
 * 
 * Use  https://github.com/nrk/phpiredis
 * Inpired http://php5-redis.googlecode.com/svn/trunk/Php5Redis.php
 * 
 */
class RedisConnection
{
	/**
	 * 
	 */
	protected $conn = null;

	/**
	 * Constructor.
	 *
	 * @throws RuntimeException if phpiredis extension could not be loaded.
	 */
	public function __construct()
	{
		if (!extension_loaded("phpiredis")) {
			throw new RuntimeException("PHPiredis extension could not be found. Install it https://github.com/nrk/phpiredis");
		}
	}

	public function __destruct()
	{
		try {
			$this->close();
		} catch (Exception $ex) { }
	}

	public function connect($host = '127.0.0.1', $port = 6379, $password= null)
	{
		$this->conn = @phpiredis_connect($host, $port);
		if (!$this->conn) {
			throw new Exception("Cannot connect to Redis Server on host: '$host' , using port: '$port'");
		}
                
                if(!empty($password)){
                    return $this->auth($password);
                }else{
                    return true;
                }
	}

	public function close()
	{
            $this->command(array('QUIT'));
	}

        protected function auth($password)
        {
            return $this->command(array('AUTH '.$password));
        }


        public function command($command /* [array $command]*/){
            
            if (empty($this->conn)){
                throw new Exception("You need to conect before send commands to the server.");
            }
            
            if (is_array($command) && is_array($command[0]) ){
                return phpiredis_multi_command_bs($this->conn, $command);
            }elseif (!empty($command)) {
                return phpiredis_command_bs($this->conn, $command) ;
            }else{
                throw new Exception('Command can not be empty.');
            }
        }
        
	public function set($key, $value, $overWrite = true)
	{
            if (!is_string($value))
                $value= serialize ($value);
		
            return $this->command(array(($overWrite)?'SET':'SETNX', $key, $value) );
                
	}

	public function get($key)
	{
		
            $rst= $this->command( array('GET', $key) );
                
            if( $this->isSerialized($rst) )
                return unserialize ($rst);
            else return $rst;
            
	}
        
        function __get($key)
        {
            
            return $this->get ($key);
                
	}
        
        function __set($key, $value)
        {
            
            return $this->set ( $key, $value );
            
	}
        
        public function expire($key, $TTL)
	{
		
            return $this->command( array('EXPIRE', $key, $TTL) );
                
	}
        
        function ttl($key)
        {
            
            return $this->command (array('TTL', $key) );
            
	}
        
        public function delete($key)
        {
            
            return $this->command ( array('DEL', $key) );
            
	}
        
        function exists($key)
        {
            
            return $this->command ( array('EXISTS', $key) );
            
	}
        
        
        function getAllKeys($key)
        {
            
            return $this->command ( array('KEYS *') );
            
	}
        protected function isSerialized( $data )
        {
            // if it isn't a string, it isn't serialized
            if ( !is_string( $data ) )
                return false;
            $data = trim( $data );
            if ( 'N;' == $data )
                return true;
            if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
                return false;
            switch ( $badions[1] ) {
                case 'a' :
                case 'O' :
                case 's' :
                    if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
                        return true;
                    break;
                case 'b' :
                case 'i' :
                case 'd' :
                    if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
                        return true;
                    break;
            }
            return false;
        }
}
?>
