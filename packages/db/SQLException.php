<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */


//namespace db;

/**
 * An exception that provides information on a database access error.
 *
 * @exception
 * @package db
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
class SQLException extends Exception
{
	/**
	 * @var integer
	 */
	protected $errorCode;
	/**
	 * @var string
	 */
	protected $sql;

	/**
	 * Constructor
	 *
	 * @param string $msg
	 * @param int $errorCode Database specific error code.
	 * @param string $sql sql statement that caused the exception
	 */
	function __construct($msg = '', $errorCode = 0, $sql = '')
	{
		parent::__construct($msg);
		$this->errorCode = $errorCode;
		$this->sql = preg_replace('#[\t ]+#', ' ', $sql);
	}
	/**
	 * Get database specific error code.
	 *
	 * @return int
	 */
	public function getErrorCode()
	{
		return $this->errorCode;
	}

	/**
	 * Get the last sql statement that caused the exception
	 *
	 * @return string
	 */
	public function getSQL()
	{
		return $this->sql;
	}

	public function  __toString()
	{
		$str = parent::__toString();
		if ($this->sql) $str .= "\nSQL:\n" . $this->sql;
		return $str;
	}
}
?>
