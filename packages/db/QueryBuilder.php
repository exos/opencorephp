<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 * @
 */

// namespace db;

import("db.DB");

/**
 * Class for creating sql statements
 *
 * @package db
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
class QueryBuilder {

	/**
	 * @var int
	 */
	private $_bindIndex;
	/**
	 * @var mixed[]
	 */
	private $_bindValues = array();
	/**
	 * @var Connection
	 */
	private $_conn;

	private function _bindQueryCallback($match) {
		if (!array_key_exists($this->_bindIndex, $this->_bindValues)) {
			$index = $this->_bindIndex+1;
			throw new SQLException("Invalid number of supplied parameters. Missing value for placeholder #{$index}");
		}
		$value = $this->php2Sql($this->_bindValues[$this->_bindIndex++]);
		return $value;
	}

	/**
	 * Create a SQL query using the supplied field names and values.
	 *
	 * @param string $type Valid types are: INSERT, REPLACE, UPDATE
	 * @param string $table Table name
	 * @param array $fields Keys are column names
	 * @param string $where
	 * @param string $fieldPrefix prefix for column names
         * @param bool $delayed for inserts delayed.
	 * @return string
	 */
	private function _createSql($type, $table = null, array $fields, $where = '', $fieldPrefix = '', $delayed = false) {
                
                /**
                 *  @TODO: Escape tables names!
                 */
            
                if (!is_null($table)) {
                    $table = "{$table}  SET ";
                } else {
                    $table = '';
                }
            
                $delayedSql = $delayed ? 'DELAYED' : '';
		$sql = ($type == 'INSERT' || $type == 'REPLACE') ? "$type $delayedSql INTO $table" : "UPDATE $table";
		$parts = array();
		foreach ($fields as $field => $value) {
			$parts[] = $fieldPrefix . $field . " = " . $this->php2Sql($value);
		}
		$sql .= implode(", ", $parts);
		if ($where) $sql .= " WHERE $where";
                
		return $sql;
	}

	/**
	 * Create an instance of this class. Shortcut for method chaining.
	 *
	 * @param string|Connection $conn A Connection instance or a connection name. If NULL 'default' connection will be used.
	 * @return QueryBuilder
	 */
	public static function create($conn = null) {
		return new self($conn);
	}

	/**
	 * Constructor.
	 *
	 * @param string|Connection $conn A Connection instance or a connection name. If NULL 'default' connection will be used.
	 * @see DB#getConnection
	 */
	public function  __construct($conn = null) {
		if ($conn instanceof Connection) {
			$this->_conn = $conn;
		}
		else {
			if (!$conn) $this->_conn = DB::getConnection();
			else $this->_conn = DB::getConnection($conn);
		}
	}

	/**
	 * Convert php value to sql value according data type and connection.
	 * Strings are escaped and quoted and arrays are converted to a comma separated list between parenthesis.
	 * Other types are left untouched.
	 *
	 * @param mixed $value
	 * @return mixed
	 * @throws SQLException
	 */
	public function php2Sql($value) {
		if (is_string($value)) return "'" . $this->_conn->quote($value) . "'";
		else if (is_array($value)) return '(' . implode(',', array_map(array($this, 'php2Sql'), $value)) . ')';
		else return var_export($value, true);
	}

	/**
	 * Replace questions marks with the specified values. Order is preserved.
	 * Values are mapped from php to mysql.
	 * Strings are automatically escaped and arrays are converted to a comma separated list between parenthesis.
	 * WARNING: Empty arrays still returns "()"
	 *
	 * @param string $sql
	 * @param array $values
	 * @return string
	 * @throws SQLException
	 */
	public function bind($sql, array $values) {
		$this->_bindIndex = 0;
		$this->_bindValues = $values;
		$sql = preg_replace_callback('#\?#', array($this, '_bindQueryCallback'), $sql, -1);
		return $sql;
	}

	/**
	 * Create an INSERT statement.
	 *
	 * @param string $table Db table
	 * @param array $fields Array where keys are column names.
	 * @param string $fieldPrefix prefix for column names.
	 * @param bool $delayed for inserts delayed.
	 * @return string
	 */
	public function createInsert($table, array $fields, $fieldPrefix = '', $delayed = false) {
		return $this->_createSql('INSERT', $table, $fields, '', $fieldPrefix, $delayed);
	}
        
        /**
	 * Create an INSERT or UPDATE
	 *
	 * @param string $table Db table
	 * @param array $fields Array where keys are column names.
	 * @param string $fieldPrefix prefix for column names.
	 * @param bool $delayed for inserts delayed.
	 * @return string
	 */
	public function createInsertOrUpdate($table, array $fields, $fieldPrefix = '', $delayed = false) {
		$sql =  $this->_createSql('INSERT', $table, $fields, '', $fieldPrefix);
                $sql .= " ON DUPLICATE KEY ";
                $sql .= $this->_createSql('UPDATE', null, $fields,'',$fieldPrefix);
                return $sql;
	}

	/**
	 * Create a REPLACE statement.
	 *
	 * @param string $table Db table
	 * @param array $fields Array where keys are column names.
	 * @param string $fieldPrefix prefix for column names.
	 * @return string
	 */
	public function createReplace($table, array $fields, $fieldPrefix = '') {
		return $this->_createSql('REPLACE', $table, $fields, '', $fieldPrefix);
	}

	/**
	 * Create an UPDATE statement.
	 *
	 * @param string $table Db table
	 * @param array $fields Array where keys are column names.
	 * @param string $where SQL WHERE as string excluding the WHERE keyword.
	 * @param string $fieldPrefix prefix for column names.
	 * @return string
	 */
	public function createUpdate($table, array $fields, $where = '', $fieldPrefix = '') {
		return $this->_createSql('UPDATE', $table, $fields, $where, $fieldPrefix);
	}
    
    /**
     * TODO
     * @param type $table
     * @param array $fields
     * @param type $joins
     * @param type $where
     * @param type $fieldPrefix
     * @param type $options 
     */
    public function createQuery($table, $fields = "*", $joins = null, $where = '', $fieldPrefix = '', $options = null) {
		
        // TODO
//        
//        $sql = "SELECT ";
//        
//        $sql .= " $fields ";
//        
//        $sql .= " FROM $table $alias ";
//        
//        $sql .= $joins;
//        
//        $sql .= ' WHERE '.$conditions;
//        
//        $sql .= " GROUP BY $group_criteria"; 
//        
//        $sql .= " ORDER BY $pagiOrder"; 
//        
//        $sql .= " LIMIT $limit";
	}
    
    /**
     * TODO
     * 
     * @param type $table
     * @param type $where
     * @param type $fieldPrefix
     */
    public function createDelete($table, $where = '', $fieldPrefix = '') {
        
        // TODO
    }
}
?>
