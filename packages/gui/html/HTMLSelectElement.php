<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */

//namespace gui\html;

import('gui.html.HTMLFormElement');

/**
 * This class renders a form select element.
 *
 * @package gui.html
 * @author Demián Andrés Rodriguez (demian85@gmail.com)
 */
class HTMLSelectElement extends HTMLFormElement {

	protected $options = array();
	protected $selectedOption = null;
	protected $keyAsValue = true;

	public function  __construct($name = null, $id = null, array $options = array()) {
		parent::__construct($name);
		$this->setId($id);
		$this->options = $options;
	}

	public function addOption($value, $text = '') {
		$this->options[$value] = $text;
		return $this;
	}

	public function addOptions(array $options) {
		$this->options += $options;
		return $this;
	}

	public function setOptions(array $options) {
		$this->options = $options;
		return $this;
	}

	public function setKeyAsValue() {
		$this->keyAsValue = $keyAsValue;
		return $this;
	}

	public function setValue($value) {
		$this->selectedOption = $selected;
		return $this;
	}

	public function render() {
		return HTML::select($this->getAttr('name'), $this->getAttr('id'), $this->options, $this->selectedOption, $this->keyAsValue, $this->_getAttrs());
	}
}
?>
