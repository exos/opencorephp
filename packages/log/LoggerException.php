<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */


//namespace log;

/**
 * Exception thrown by the Logger class. Indicates any type of log error.
 * If automatic exception logging is enabled, this type of exception will never be logged.
 *
 * @exception 
 * @package util
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
class LoggerException extends Exception
{
	public function __construct($message, Exception $cause = null)
	{
		if ($cause) $message .= "\n\nCause: (" . get_class($cause) . ") " . $cause->getMessage();
		parent::__construct($message);
	}
}

?>