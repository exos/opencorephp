<?php

class HTMLParser {


	/**
	 * HTMLParser
	 * 
	 * @param string $s_str
	 * @return array
	 */
	public static function Parse( $s_str )
	{
			$i_indicatorL = 0;
			$i_indicatorR = 0;
			$s_tagOption = "";
			$i_arrayCounter = 0;
			$a_html = array();
			// Search for a tag in string
			while( is_int(($i_indicatorL=strpos($s_str,"<",$i_indicatorR))) ) {
					// Get everything into tag...
					$i_indicatorL++;
					$i_indicatorR = strpos($s_str,">", $i_indicatorL);
					$s_temp = substr($s_str, $i_indicatorL, ($i_indicatorR-$i_indicatorL) );
					$a_tag = explode( ' ', $s_temp );
					// Here we get the tag's name
					list( ,$s_tagName,, ) = each($a_tag);
					$s_tagName = strtolower($s_tagName);
					// Well, I am not interesting in <br>, </font> or anything else like that...
					// So, this is false for tags without options.
					$b_boolOptions = is_array(($s_tagOption=each($a_tag))) && $s_tagOption[1];
					if( $b_boolOptions ) {
						// Without this, we will mess up the array
						if (isset($a_html[$s_tagName]))
							$i_arrayCounter = intval(count($a_html[$s_tagName]));
						else $i_arrayCounter=0;
						// get the tag options, like src="htt://". Here, s_tagTokOption is 'src'
						//and s_tagTokValue is '"http://"'

						do {
								$s_tagTokOption = strtolower(strtok($s_tagOption[1], "="));
								$s_tagTokValue = trim(strtok("="));

								if (substr($s_tagTokValue,0,1)=='"' || substr($s_tagTokValue,0,1)=="'")
									$s_tagTokValue=substr($s_tagTokValue,1);

								if (substr($s_tagTokValue,-1)=='"' || substr($s_tagTokValue,-1)=="'")
									$s_tagTokValue=substr($s_tagTokValue,0,-1);
							
								$a_html[$s_tagName][$i_arrayCounter][$s_tagTokOption] =
								$s_tagTokValue;
								$b_boolOptions = is_array(($s_tagOption=each($a_tag))) &&
								$s_tagOption[1];
						 } while( $b_boolOptions );
					}
			}
			return $a_html;
	}


	/**
    * Función para limpiar cualquier meta tag o cadena.
    * Lo limpia de caracteres htmlentities tipo &aacute, &gt; etc
    * Le quita los saltos de linea de html, los <br /> y cualquier carácter de tabulacion o que le añada espacio.
    * Adicionalmente se le puede pasar un numero de caracteres que se quiere mostrar, de esta manera el string se corta por ese numero
    *
    * @param mixed $string
    * @param mixed $corte
    * @return string
    */
    public static function clean_metas($string,$corte = null)
    {
        $caracters_no_permitidos = array("\"","'");
        # paso los caracteres entities tipo &aacute; $gt;etc a sus respectivos html
        $s = html_entity_decode($string,ENT_COMPAT,'UTF-8');
        # quito todas las etiquetas html y php
        $s = strip_tags($s);
        # elimino todos los retorno de carro
        $s = str_replace("\r", '', $s);
        # en todos los espacios en blanco le añado un <br /> para después eliminarlo
        $s = preg_replace('/(?<!>)\n/', "<br />\n", $s);
        # elimino la inserción de nuevas lineas
        $s = str_replace("\n", '', $s);
        # elimino tabulaciones y el resto de la cadena
        $s = str_replace("\t", '', $s);
        # elimino caracteres en blanco
        $s = preg_replace('/[ ]+/', ' ', $s);
        $s = preg_replace('/<!--[^-]*-->/', '', $s);
        # vuelvo a hacer el strip para quitar el <br /> que he añadido antes para eliminar las saltos de carro y nuevas lineas
        $s  = strip_tags($s);
        # elimino los caracters como comillas dobles y simples
        $s = str_replace($caracters_no_permitidos,"",$s);

        if (isset($corte) && (is_numeric($corte)))
        {
            $s = mb_substr($s,0,$corte, 'UTF-8');
        }

        return $s;
    }

	
}


?>
