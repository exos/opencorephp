<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */

/**
 * Example of a typical registration controller.
 * @author ZedPlan Team (opencorephp@zedplan.com)
 *
 */
class RegisterController extends Controller
{
	protected $input = null;
	
	protected function _getInput() {
		if (!$this->input) {
			$this->input = new DataInput($_POST);
			$this->input->init(array('username', 'email', 'fname', 'lname', 'password', 'password2'), 'string');
		}
		return $this->input;
	}
	protected function _getForm() {
		$view = new DocumentView('user/register/form', _("Register a new account"));
		$view->data = $this->_getInput();
		$view->errors = array();
		return $view;
	}
	protected function _validate() {
		$input = $this->_getInput();
		$input->validate('username', 'string', _('Invalid username'));
		$input->validate('email', 'email', _('Invalid email'));
		$input->validate('fname', 'string', _('Invalid first name'));
		$input->validate('lname', 'string', _('Invalid last name'));
		$input->validate('password', 'string', _('Invalid password'));
		$errors = $input->getErrors();
		if ($input['password'] != $input['password2']) {
			$errors[] = _("Passwords do not match");
		}
		if (User::usernameExists($input['username'])) {
			$errors[] = _("Your username already exists in our database");
		}
		if (User::emailExists($input['email'])) {
			$errors[] = _("Your email already exists in our database");
		}
		
		return $errors;
	}
	
	function __construct() {
		parent::__construct();
		$this->defaultAction = $this->request->isPost() ? 'process' : 'form';
	}
	
	function formAction() {
		echo $this->_getForm();
	}
	
	function processAction() {
		$errors = $this->_validate();
		
		if (empty($errors)) {
			// add
			$input = $this->input;
			User::add($input['username'], $input['email'], $input['password'], $input['fname'],	$input['lname']);
			$this->redirect("/");
		}
		else {
			$view = $this->_getForm();
			$view->errors = $errors;
			echo $view;
		}
	}
}
?>