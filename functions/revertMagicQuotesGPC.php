<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */



/**
 * Revert magic_quotes_gpc (if active) by stripping slashes from supeglobal vars _GET, _POST and _COOKIE
 *
 * @return void
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
function revertMagicQuotesGPC() {
	if (ini_get('magic_quotes_gpc') == 1) {
		function _revertMagicQuotesGPC(&$value, $key) {
			$value = stripslashes($value);
		}
		// TODO Lambda function
		array_walk_recursive($_GET, '_revertMagicQuotesGPC');
		array_walk_recursive($_POST, '_revertMagicQuotesGPC');
		array_walk_recursive($_COOKIE, '_revertMagicQuotesGPC');
	}
}
?>