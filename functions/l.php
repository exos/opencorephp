<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2012, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2012, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */



/**
 * Alias of Lang::get()
 * 
 * @param string $phrase A single phrase.
 * @param string $catalog NULL indicates default catalog extracted from {i18n.default_catalog}
 * @param string $locale NULL indicates default locale extracted from {app.locale}
 * @return string
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
function l($phrase, $catalog = null, $locale = null) {
	return Lang::getInstance()->get($phrase, $catalog, $locale);
}

?>